# DiasporaApiTestHarness

A test harness application for interacting with the Diaspora API. 

This can be used locally by building and doing a mvn install such as:

```bash
mvn install:install-file -Dfile=harness-1.0-SNAPSHOT.jar -DgroupId=hankg.diaspora.api -DartifactId=harness -Dversion=1.0-SNAPSHOT -Dpackaging=jar
``` 