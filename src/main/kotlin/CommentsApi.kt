package hankg.diaspora.api

import com.github.kittinunf.fuel.core.Method
import com.github.kittinunf.fuel.util.FuelRouting
import com.google.gson.Gson

sealed class CommentsApi(val postGuid: String, val settings:Settings) : FuelRouting {
    override val basePath = settings.baseUrl

    class getPostComments(postGuid: String, settings:Settings): CommentsApi(postGuid, settings) {}
    class addComment(postGuid: String, val comment:String, settings:Settings): CommentsApi(postGuid, settings) {}
    class deleteComment(postGuid: String, val commentGuid:String, settings:Settings): CommentsApi(postGuid, settings) {}
    class reportComment(postGuid: String, val commentGuid:String, val reason:String, settings:Settings): CommentsApi(postGuid, settings) {}

    override val method: Method
        get() {
            when(this) {
                is getPostComments -> return Method.GET
                is addComment -> return Method.POST
                is deleteComment -> return Method.DELETE
                is reportComment -> return Method.POST
            }
        }

    override val path: String
        get() {
            when(this) {
                is getPostComments -> return "posts/${this.postGuid}/comments?access_token=${this.settings.accessToken}"
                is addComment -> return "posts/${this.postGuid}/comments?access_token=${this.settings.accessToken}"
                is deleteComment -> return "posts/${this.postGuid}/comments/${this.commentGuid}?access_token=${this.settings.accessToken}"
                is reportComment -> return "posts/${this.postGuid}/comments/${this.commentGuid}/report?access_token=${this.settings.accessToken}"
            }
        }

    override val params:List<Pair<String,Any?>>?
        get() = null

    override val headers: Map<String, String>?
        get() = mapOf("Content-Type" to "application/json")

    override val body: String?
        get() {
            if(this is addComment) {
                return Gson().toJson(mapOf("body" to this.comment))
            } else if(this is reportComment) {
                return Gson().toJson(mapOf("reason" to this.reason))
            } else {
                return null
            }
        }
}