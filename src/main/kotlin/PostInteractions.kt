package hankg.diaspora.api

import com.github.kittinunf.fuel.core.Method
import com.github.kittinunf.fuel.util.FuelRouting
import com.google.gson.Gson

sealed class PostInteractions(val postGuid:String, val settings:Settings) : FuelRouting {
    override val basePath = "${this.settings.baseUrl}/posts/${this.postGuid}"

    class mute(postGuid: String, settings:Settings) : PostInteractions(postGuid, settings) {}
    class report(postGuid: String, val reason: String, settings:Settings) : PostInteractions(postGuid, settings) {}
    class subscribe(postGuid: String, settings:Settings) : PostInteractions(postGuid, settings) {}
    class toggleHide(postGuid: String, settings:Settings) : PostInteractions(postGuid, settings) {}
    class vote(postGuid: String, val answerId:String, settings:Settings) : PostInteractions(postGuid, settings) {}

    override val method: Method
        get() {
            when (this) {
                is toggleHide -> return Method.POST
                is mute -> return Method.POST
                is report -> return Method.POST
                is subscribe -> return Method.POST
                is vote -> return Method.POST
            }
        }

    override val path: String
        get() {
            when (this) {
                is toggleHide -> return "hide?access_token=${this.settings.accessToken}"
                is mute -> return "mute?access_token=${this.settings.accessToken}"
                is report -> return "report?access_token=${this.settings.accessToken}"
                is subscribe -> return "subscribe?access_token=${this.settings.accessToken}"
                is vote -> return "vote?access_token=${this.settings.accessToken}"
            }
        }

    override val params: List<Pair<String, Any?>>?
        get() = null

    override val headers: Map<String, String>?
        get() = mapOf("Content-Type" to "application/json")

    override val body: String?
        get() {
            if(this is report) {
                return Gson().toJson(mapOf("reason" to this.reason))
            } else if(this is vote) {
                return Gson().toJson(mapOf("poll_answer_id" to this.answerId))
            } else {
                return null
            }
        }

}