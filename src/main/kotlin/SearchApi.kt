package hankg.diaspora.api

import com.github.kittinunf.fuel.core.Method
import com.github.kittinunf.fuel.util.FuelRouting

sealed class SearchApi(val settings:Settings) : FuelRouting {
    override val basePath = settings.baseUrl

    class searchUsersByName(val name:String = "", settings:Settings): SearchApi(settings) {}
    class searchUsersByTag(val tag:String ="", settings:Settings): SearchApi(settings) {}
    class searchPosts(val tag:String = "", settings:Settings): SearchApi(settings) {}

    override val method: Method
        get() = Method.GET

    override val path: String
        get() = when(this) {
            is searchUsersByName -> "search/users"
            is searchUsersByTag -> "search/users"
            is searchPosts -> "search/posts"
        }

    override val params:List<Pair<String,Any?>>?
        get() = when(this){
            is searchUsersByName -> listOf("access_token" to this.settings.accessToken, "name_or_handle" to this.name)
            is searchUsersByTag -> listOf("access_token" to this.settings.accessToken, "tag" to this.tag)
            is searchPosts -> listOf("access_token" to this.settings.accessToken, "tag" to this.tag)
        }

    override val headers: Map<String, String>?
        get() = mapOf("Content-Type" to "application/json")

    override val body: String?
        get() = null
}