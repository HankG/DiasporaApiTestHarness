package hankg.diaspora.api

import com.github.kittinunf.fuel.Fuel

fun main(args : Array<String>) {
    println("access_token = $globalSettings")

    val postGuid = "2ae87b40b3ae0136d9b1080027f3b408"

    val (_, _, result) = Fuel.request(LikesApi.showLikes(postGuid, globalSettings)).responseObject(Like.ArrayDeserializer())
    println(result)

    println("Done")
}
