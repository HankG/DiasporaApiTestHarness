package hankg.diaspora.api

import com.github.kittinunf.fuel.core.Method
import com.github.kittinunf.fuel.util.FuelRouting
import com.google.gson.Gson

sealed class TagFollowingsApi(val settings:Settings) : FuelRouting {
    override val basePath = settings.baseUrl

    class getList(settings:Settings): TagFollowingsApi(settings) {}
    class follow(val tag:String, settings:Settings): TagFollowingsApi(settings) {}
    class unfollow(val tag:String, settings:Settings): TagFollowingsApi(settings) {}

    override val method: Method
        get() = when(this) {
            is getList -> Method.GET
            is follow -> Method.POST
            is unfollow -> Method.DELETE
        }

    override val path: String
        get() = when(this) {
            is getList -> "tag_followings"
            is follow -> "tag_followings?access_token=${this.settings.accessToken}"
            is unfollow -> "tag_followings/${this.tag}"
        }

    override val params:List<Pair<String,Any?>>?
        get() = listOf("access_token" to this.settings.accessToken)


    override val headers: Map<String, String>?
        get() = mapOf("Content-Type" to "application/json")

    override val body: String?
        get() {
            if(this is follow) {
                return Gson().toJson(mapOf("name" to this.tag))
            } else {
                return null
            }
        }
}