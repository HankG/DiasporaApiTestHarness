package hankg.diaspora.api

import com.github.kittinunf.fuel.core.Method
import com.github.kittinunf.fuel.util.FuelRouting
import com.google.gson.Gson

sealed class ContactsApi(val aspectGuid: String, val settings:Settings) : FuelRouting {
    override val basePath = settings.baseUrl

    class getContacts(aspectGuid: String, settings:Settings): ContactsApi(aspectGuid, settings) {}
    class addContact(aspectGuid: String, val person:Person, settings:Settings): ContactsApi(aspectGuid, settings) {}
    class deleteContact(aspectGuid: String, val person:Person, settings:Settings): ContactsApi(aspectGuid, settings) {}

    override val method: Method
        get() {
            when(this) {
                is getContacts -> return Method.GET
                is addContact -> return Method.POST
                is deleteContact -> return Method.DELETE
            }
        }

    override val path: String
        get() {
            when(this) {
                is getContacts -> return "aspects/${this.aspectGuid}/contacts"
                is addContact -> return "aspects/${this.aspectGuid}/contacts?access_token=${this.settings.accessToken}"
                is deleteContact -> return "aspects/${this.aspectGuid}/contacts/${this.person.guid}"
             }
        }

    override val params:List<Pair<String,Any?>>?
        get() = listOf("access_token" to this.settings.accessToken)

    override val headers: Map<String, String>?
        get() = mapOf("Content-Type" to "application/json")

    override val body: String?
        get() {
            val bodyParams = mutableMapOf<String, Any?>()
            when(this) {
                is addContact -> bodyParams["person_guid"] = this.person.guid
                else -> {}
            }
            return Gson().toJson(bodyParams)
        }
}