package hankg.diaspora.api

import com.github.kittinunf.fuel.core.Method
import com.github.kittinunf.fuel.util.FuelRouting

sealed class PhotosApi(val settings:Settings) : FuelRouting {
    override val basePath = settings.baseUrl

    class delete(val photoGuid:String, settings:Settings): PhotosApi(settings) {}
    class info(val photoGuid:String, settings:Settings): PhotosApi(settings) {}
    class list(settings:Settings): PhotosApi(settings) {}
    class upload(val pending:Boolean = false, val profilePic:Boolean = false,
                 val aspects:List<Aspect> = listOf<Aspect>(), settings:Settings): PhotosApi(settings) {
        val fullPath = "$basePath/$path"
    }

    override val method: Method
        get() {
            when(this) {
                is delete -> return Method.DELETE
                is info -> return Method.GET
                is list -> return Method.GET
                is upload -> return Method.POST
            }
        }

    override val path: String
        get() {
            when(this) {
                is delete -> return "photos/$photoGuid"
                is info -> return "photos/$photoGuid"
                is list -> return "photos"
                is upload -> return "photos"
            }
        }

    override val params:List<Pair<String,Any?>>?
        get() {
            val paramsList = mutableListOf<Pair<String, Any?>>("access_token" to this.settings.accessToken)
            if(this is upload) {
                if(this.pending) {
                    paramsList.add("pending" to this.pending)
                }

                if(this.profilePic) {
                    paramsList.add("set_profile_photo" to this.profilePic)
                }

                if(this.aspects.isNotEmpty()) {
                    paramsList.add("aspect_ids" to this.aspects.map{it.id}.toList())
                }
            }
            return paramsList.toList()
        }

    override val headers: Map<String, String>?
        get() = mapOf("Content-Type" to "application/json")

    override val body: String?
        get() = null
}