package hankg.diaspora.api

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson
import java.lang.IllegalArgumentException

data class Aspect(val id:Int = Int.MIN_VALUE,
                  val name:String = "",
                  val order:Int = 0,
                  val chat_enabled:Boolean = false) {
    class Deserializer : ResponseDeserializable<Aspect> {
        override fun deserialize(content: String): Aspect = Gson().fromJson(content, Aspect::class.java)
    }

    class ArrayDeserializer : ResponseDeserializable<List<Aspect>> {
        override fun deserialize(content: String): List<Aspect> = Gson().fromJson(content, Array<Aspect>::class.java).toList()
    }

    fun fillParamList(params:MutableMap<String,Any?>, full:Boolean) {
        params["name"] = this.name
        params["chat_enabled"] = this.chat_enabled

        if(full){
            params["order"] = this.order
        }
    }
}

data class Avatar(val large:String = "",
                  val medium:String = "",
                  val small:String = "") {
    class Deserializer : ResponseDeserializable<Avatar> {
        override fun deserialize(content: String): Avatar = Gson().fromJson(content, Avatar::class.java)
    }

    class ArrayDeserializer : ResponseDeserializable<List<Avatar>> {
        override fun deserialize(content: String): List<Avatar> = Gson().fromJson(content, Array<Avatar>::class.java).toList()
    }

}

data class Comment(val guid:String = "",
                   val created_at:String = "",
                   val author:Person = Person(),
                   val body:String = "") {
    class Deserializer : ResponseDeserializable<Comment> {
        override fun deserialize(content: String): Comment = Gson().fromJson(content, Comment::class.java)
    }

    class ArrayDeserializer : ResponseDeserializable<List<Comment>> {
        override fun deserialize(content: String): List<Comment> = Gson().fromJson(content, Array<Comment>::class.java).toList()
    }

}


data class InteractionCounters(val comments:Int = 0,
                               val likes:Int = 0,
                               val reshares:Int = 0) {
    class Deserializer : ResponseDeserializable<InteractionCounters> {
        override fun deserialize(content: String): InteractionCounters = Gson().fromJson(content, InteractionCounters::class.java)
    }
}

data class Conversation(val guid:String = "",
                        val created_at:String = "",
                        val subject:String = "",
                        val read:Boolean = false,
                        val participants:List<Person> = listOf<Person>()) {
    class Deserializer : ResponseDeserializable<Conversation> {
        override fun deserialize(content: String): Conversation = Gson().fromJson(content, Conversation::class.java)
    }

    class ArrayDeserializer : ResponseDeserializable<List<Conversation>> {
        override fun deserialize(content: String): List<Conversation> = Gson().fromJson(content, Array<Conversation>::class.java).toList()
    }

}

data class ConversationMessage(val guid:String = "",
                        val created_at:String = "",
                        val body:String = "",
                        val author:Person = Person()) {
    class Deserializer : ResponseDeserializable<ConversationMessage> {
        override fun deserialize(content: String): ConversationMessage = Gson().fromJson(content, ConversationMessage::class.java)
    }

    class ArrayDeserializer : ResponseDeserializable<List<ConversationMessage>> {
        override fun deserialize(content: String): List<ConversationMessage> = Gson().fromJson(content, Array<ConversationMessage>::class.java).toList()
    }

}


data class ConversationRequest(val subject:String = "",
                               val body:String="",
                               val recipients:List<Person> = listOf<Person>()) {
    class Deserializer : ResponseDeserializable<ConversationRequest> {
        override fun deserialize(content: String): ConversationRequest = Gson().fromJson(content, ConversationRequest::class.java)
    }

    class ArrayDeserializer : ResponseDeserializable<List<ConversationRequest>> {
        override fun deserialize(content: String): List<ConversationRequest> = Gson().fromJson(content, Array<ConversationRequest>::class.java).toList()
    }

}

data class Like(val guid:String = "",
                val author:Person = Person()) {
    class Deserializer : ResponseDeserializable<Like> {
        override fun deserialize(content: String): Like = Gson().fromJson(content, Like::class.java)
    }

    class ArrayDeserializer : ResponseDeserializable<List<Like>> {
        override fun deserialize(content: String): List<Like> = Gson().fromJson(content, Array<Like>::class.java).toList()
    }
}

data class Location(val address:String = "", val lat:Double = 0.0, val lng:Double = 0.0) {
    fun isEmpty():Boolean = address.isEmpty() && lat == 0.0 && lng == 0.0

    class Deserializer : ResponseDeserializable<Location> {
        override fun deserialize(content: String): Location = Gson().fromJson(content, Location::class.java)
    }
}

data class Notification(val guid:String = "",
                        val type:NotificationType = NotificationType.unknown,
                        val read:Boolean = false,
                        val created_at:String = "",
                        val target:Target = Target(),
                        val event_creators:List<Person> = listOf()) {

    class Target(val guid:String = "",
                 val author:Person = Person()) {
        class Deserializer : ResponseDeserializable<Target> {
            override fun deserialize(content: String): Target = Gson().fromJson(content, Target::class.java)
        }
    }

    enum class NotificationType {
        also_commented,
        comment_on_post,
        liked,
        mentioned,
        mentioned_in_comment,
        reshared,
        started_sharing,
        contacts_birthday,
        unknown
    }

    class ArrayDeserializer : ResponseDeserializable<List<Notification>> {
        override fun deserialize(content: String): List<Notification> = Gson().fromJson(content, Array<Notification>::class.java).toList()
    }

    class Deserializer : ResponseDeserializable<Notification> {
        override fun deserialize(content: String): Notification = Gson().fromJson(content, Notification::class.java)
    }
}

data class NavigationData(val previous:String,
                          val next: String) {
    class Deserializer : ResponseDeserializable<NavigationData> {
        override fun deserialize(content: String): NavigationData = Gson().fromJson(content, NavigationData::class.java)
    }
}

data class OpenIdAccessTokens(val access_token:String = "",
                             val refresh_token:String = "",
                             val token_type:String = "",
                             val expires_in:Int = 0,
                             val id_token:String = "") {
    class Deserializer : ResponseDeserializable<OpenIdAccessTokens> {
        override fun deserialize(content: String): OpenIdAccessTokens = Gson().fromJson(content, OpenIdAccessTokens::class.java)
    }
}

data class PageDataUrls(val first:String="",
                        val last:String="",
                        val previous:String="",
                        val next:String="")
{
    companion object {
        fun fromHeaderLinkField(headerLinkField:String) : PageDataUrls
        {
            val mappedCursors = headerLinkField.split(",").associate {
                val ce = it.split(";")
                val url = ce.first().trim().trimStart('<').trimEnd('>')
                val cursorName = ce.last().split("=").last().trim().trimStart('"').trimEnd('"')
                Pair(cursorName, url)
            }

            return PageDataUrls(
                    mappedCursors["first"]?:"",
                    mappedCursors["last"]?:"",
                    mappedCursors["previous"]?:"",
                    mappedCursors["next"]?:""
            )
        }

        fun fromHeaderLinkField(header:Map<String,Collection<String>>) : PageDataUrls
        {
            val rawfield = header["Link"]
            val linkHeaderString = rawfield?.joinToString()?:""
            return fromHeaderLinkField(linkHeaderString)
        }
    }

    override fun toString(): String {
        val sb = StringBuffer()
        if (!first.isEmpty()) sb.append("first = $first\n")
        if (!last.isEmpty()) sb.append("last = $last\n")
        if (!previous.isEmpty()) sb.append("previous = $previous\n")
        if (!next.isEmpty()) sb.append("next = $next\n")
        return sb.toString()
    }
}

data class Person(val guid:String = "",
                  val diaspora_id:String = "",
                  val name:String = "",
                  val avatar:String = "") {


    class ArrayDeserializer : ResponseDeserializable<List<Person>> {
        override fun deserialize(content: String): List<Person> = Gson().fromJson(content, Array<Person>::class.java).toList()
    }

    class Deserializer : ResponseDeserializable<Person> {
        override fun deserialize(content: String): Person = Gson().fromJson(content, Person::class.java)
    }
}

data class Photo(val guid:String = "",
                 val post:String = "",
                 val dimensions:Dimensions = Dimensions(),
                 val sizes:PhotoSizes = PhotoSizes(),
                 val author:Person = Person()) {
    data class Dimensions(val height:Int = 0, val width: Int = 0) {
        class Deserializer : ResponseDeserializable<Dimensions> {
            override fun deserialize(content: String): Dimensions = Gson().fromJson(content, Dimensions::class.java)
        }
    }

    data class PhotoSizes(val small:String = "", val medium:String = "", val large:String = "") {
        class Deserializer : ResponseDeserializable<PhotoSizes> {
            override fun deserialize(content: String): PhotoSizes = Gson().fromJson(content, PhotoSizes::class.java)
        }
    }

    class Deserializer : ResponseDeserializable<Photo> {
        override fun deserialize(content: String): Photo = Gson().fromJson(content, Photo::class.java)
    }

    class ArrayDeserializer : ResponseDeserializable<List<Photo>> {
        override fun deserialize(content: String): List<Photo> = Gson().fromJson(content, Array<Photo>::class.java).toList()
    }

}

data class Poll(val guid:String = "",
                val participation_count:Int = 0,
                val already_participated:Boolean = false,
                val question:String = "",
                val poll_answers:List<Answer> = listOf<Answer>()) {

    fun isEmpty():Boolean = question.isEmpty() || poll_answers.isEmpty()

    fun pollRequest() = mapOf<String, Any?>("question" to this.question, "poll_answers" to poll_answers.map {it.answer})

    data class Answer(val id:String = "",
                      val answer:String = "",
                      val vote_count:Int = 0) {
        class Deserializer : ResponseDeserializable<Answer> {
            override fun deserialize(content: String): Answer = Gson().fromJson(content, Answer::class.java)
        }
    }

    class Deserializer : ResponseDeserializable<Answer> {
        override fun deserialize(content: String): Answer = Gson().fromJson(content, Answer::class.java)
    }

}

data class Post(val guid:String = "",
                val created_at:String = "",
                val post_type:String = "",
                val title:String = "",
                val body:String = "",
                val provider_display_name:String? = "",
                val public:Boolean = true,
                val nsfw:Boolean = true,
                val author:Person = Person(),
                val location:Location = Location(),
                val poll:Poll = Poll(),
                val photos:List<Photo> = listOf<Photo>(),
                val mentioned_people:List<Person> = listOf<Person>(),
                val root:ReshareData? = ReshareData(),
                val interaction_counters:InteractionCounters = InteractionCounters()) {

    fun fillNewPostRequestData(postParams: MutableMap<String, Any?>, aspects:List<Aspect>) {
        if (this.body.isEmpty() && this.poll.isEmpty() && this.photos.isEmpty()) {
            throw IllegalArgumentException("Posts need to have a body, a poll, or photos")
        }

        postParams["body"] = this.body
        postParams["public"] = this.public
        if (!this.public) {
            if (aspects.isEmpty()) {
                throw IllegalArgumentException("non-public post needs to have aspects listed")
            }
            postParams["aspects"] = aspects.map{it.id.toString()}.toList()
        }

        if(!this.location.isEmpty()) {
            postParams["location"] = this.location
        }

        if(!this.poll.isEmpty()) {
            postParams["poll"] = this.poll.pollRequest()
        }
    }

    class Deserializer : ResponseDeserializable<Post> {
        override fun deserialize(content: String): Post = Gson().fromJson(content, Post::class.java)
    }

    class ArrayDeserializer : ResponseDeserializable<List<Post>> {
        override fun deserialize(content: String): List<Post> = Gson().fromJson(content, Array<Post>::class.java).toList()
    }
}


data class ReshareData(val guid: String = "",
                       val created_at: String = "",
                       val author: Person = Person()) {
    class Deserializer : ResponseDeserializable<ReshareData> {
        override fun deserialize(content: String): ReshareData = Gson().fromJson(content, ReshareData::class.java)
    }

    class ArrayDeserializer : ResponseDeserializable<List<ReshareData>> {
        override fun deserialize(content: String): List<ReshareData> = Gson().fromJson(content, Array<ReshareData>::class.java).toList()
    }
}

class StringArrayDeserializer : ResponseDeserializable<List<String>> {
    override fun deserialize(content: String): List<String> = Gson().fromJson(content, Array<String>::class.java).toList()
}


data class User(val guid:String = "",
                val name:String = "",
                val searchable:Boolean = true,
                val show_profile_info:Boolean = true,
                val birthday:String = "",
                val diaspora_id:String = "",
                val gender:String = "",
                val location:String = "",
                val bio:String = "",
                val nsfw:Boolean = false,
                val avatar:Avatar = Avatar(),
                val tags:List<String> = listOf()) {


    class ArrayDeserializer : ResponseDeserializable<List<User>> {
        override fun deserialize(content: String): List<User> = Gson().fromJson(content, Array<User>::class.java).toList()
    }

    class Deserializer : ResponseDeserializable<User> {
        override fun deserialize(content: String): User = Gson().fromJson(content, User::class.java)
    }
}
