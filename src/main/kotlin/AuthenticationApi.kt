package hankg.diaspora.api

import com.github.kittinunf.fuel.core.Method
import com.github.kittinunf.fuel.util.FuelRouting

sealed class AuthenticationApi(val settings:Settings) : FuelRouting {
    override val basePath = "${settings.server}/api/openid_connect"

    class refresh(val refreshToken:String, val clientId:String, val clientSecret:String,
                  settings:Settings): AuthenticationApi(settings) {}

    override val method: Method
        get() {
            when(this) {
                is refresh -> return Method.POST
             }
        }

    override val path: String
        get() {
            when(this) {
                is refresh -> return "access_tokens?grant_type=refresh_token&refresh_token=$refreshToken&client_id=$clientId&client_secret=$clientSecret"
            }
        }

    override val params:List<Pair<String,Any?>>?
        get() = null

    override val headers: Map<String, String>?
        get() = mapOf("Content-Type" to "application/json")

    override val body: String?
        get() = null

}