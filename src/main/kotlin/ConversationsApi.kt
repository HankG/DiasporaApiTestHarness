package hankg.diaspora.api

import com.github.kittinunf.fuel.core.Method
import com.github.kittinunf.fuel.util.FuelRouting
import com.google.gson.Gson
import java.time.LocalDateTime
import java.time.ZonedDateTime

sealed class ConversationsApi(val settings:Settings) : FuelRouting {
    override val basePath = settings.baseUrl

    class addConversation(val newConversation:ConversationRequest, settings:Settings): ConversationsApi(settings) {}
    class addMessage(val conversation:Conversation, val text:String, settings:Settings): ConversationsApi(settings) {}
    class ignoreAndHide(val guid:String, settings:Settings): ConversationsApi(settings) {}
    class list(val unreadOnly:Boolean = false,
               val updatedAfterOnly:Boolean = false,
               val afterTime:ZonedDateTime = ZonedDateTime.now(),
               settings:Settings): ConversationsApi(settings) {}
    class getConversation(val guid:String, settings:Settings): ConversationsApi(settings) {}
    class getMessages(val conversation:Conversation, settings:Settings): ConversationsApi(settings) {}

    override val method: Method
        get() {
            return when(this) {
                is addConversation -> Method.POST
                is addMessage -> Method.POST
                is getConversation -> Method.GET
                is getMessages -> Method.GET
                is ignoreAndHide -> Method.DELETE
                is list -> Method.GET
            }
        }

    override val path: String
        get() {
            return when(this) {
                is addConversation -> "conversations?access_token=${this.settings.accessToken}"
                is addMessage -> "conversations/${this.conversation.guid}/messages?access_token=${this.settings.accessToken}"
                is getConversation -> "conversations/${this.guid}"
                is getMessages -> "conversations/${this.conversation.guid}/messages"
                is ignoreAndHide -> "conversations/${this.guid}"
                is list -> "conversations"
            }
        }

    override val params:List<Pair<String,Any?>>?
        get() {
            return when(this) {
                is addConversation -> listOf("access_token" to this.settings.accessToken)
                is addMessage -> listOf("access_token" to this.settings.accessToken)
                is getConversation -> listOf("access_token" to this.settings.accessToken)
                is ignoreAndHide -> listOf("access_token" to this.settings.accessToken)
                is getMessages -> listOf("access_token" to this.settings.accessToken)
                is list -> {
                    val paramsList = mutableListOf<Pair<String,Any?>>()
                    paramsList.add("access_token" to this.settings.accessToken)
                    if(this.unreadOnly) {
                        paramsList.add("only_unread" to true)
                    }

                    if(this.updatedAfterOnly) {
                        paramsList.add("only_after" to this.afterTime.toString())
                    }
                    paramsList.toList()
                }
            }
        }

    override val headers: Map<String, String>?
        get() = mapOf("Content-Type" to "application/json")

    override val body: String?
        get() {
            if(this is addMessage) {
                return Gson().toJson(mapOf("body" to this.text))
            } else if (this is addConversation) {
                return Gson().toJson(mapOf(
                    "body" to this.newConversation.body,
                    "subject" to this.newConversation.subject,
                    "recipients" to this.newConversation.recipients.map {it.guid}
                ))
            } else {
                return null
            }
        }
}