package hankg.diaspora.api

import com.github.kittinunf.fuel.core.Method
import com.github.kittinunf.fuel.util.FuelRouting
import com.google.gson.Gson
import java.time.LocalDateTime
import java.time.ZonedDateTime

sealed class NotificationsApi(val settings:Settings) : FuelRouting {
    override val basePath = settings.baseUrl

    class list(val unreadOnly:Boolean = false,
               val onlyAfter:Boolean = false,
               val type:Notification.NotificationType = Notification.NotificationType.unknown,
               val afterDate:ZonedDateTime = ZonedDateTime.now(),
               settings:Settings): NotificationsApi(settings) {}
    class getInfo(val notificationGuid: String, settings:Settings): NotificationsApi(settings) {}
    class markReadUnread(val notificationGuid: String, val status:Boolean = true,
                         settings:Settings): NotificationsApi(settings) {}

    override val method: Method
        get() {
            when(this) {
                is list -> return Method.GET
                is getInfo -> return Method.GET
                is markReadUnread -> return Method.PATCH
            }
        }

    override val path: String
        get() {
            when(this) {
                is list -> return "notifications"
                is getInfo -> return "notifications/$notificationGuid"
                is markReadUnread -> return "notifications/$notificationGuid?access_token=${this.settings.accessToken}"
            }
        }

    override val params:List<Pair<String,Any?>>?
        get() {
            when(this) {
                is list -> {
                    val paramSet = mutableListOf<Pair<String, Any?>>()
                    paramSet.add("access_token" to this.settings.accessToken)
                    if(unreadOnly) {
                        paramSet.add("only_unread" to this.unreadOnly)
                    }

                    if(type != Notification.NotificationType.unknown) {
                        paramSet.add("type" to this.type)
                    }

                    if(onlyAfter) {
                        paramSet.add("only_after" to this.afterDate.toOffsetDateTime().toString())
                    }

                    return paramSet.toList()
                }
                is getInfo -> return listOf("access_token" to this.settings.accessToken)
                else -> return null
            }
        }

    override val headers: Map<String, String>?
        get() = mapOf("Content-Type" to "application/json")

    override val body: String?
        get() = when(this) {
            is markReadUnread -> {
                val bodyParams = mapOf<String, Any?>("read" to this.status)
                Gson().toJson(bodyParams)
            }
            else -> null
        }
}