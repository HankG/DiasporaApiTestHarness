package hankg.diaspora.api

import com.github.kittinunf.fuel.core.Method
import com.github.kittinunf.fuel.util.FuelRouting
import com.google.gson.Gson

sealed class UsersApi(val settings:Settings) : FuelRouting {
    override val basePath = settings.baseUrl

    class getContacts(val userGuid:String, settings:Settings): UsersApi(settings) {}
    class getCurrentUser(settings:Settings): UsersApi(settings) {}
    class getPhotos(val userGuid:String, settings:Settings): UsersApi(settings) {}
    class getPosts(val userGuid:String, settings:Settings): UsersApi(settings) {}
    class updateCurrentUser(val user:User, settings:Settings): UsersApi(settings) {}
    class getUser(val userGuid:String, settings:Settings): UsersApi(settings) {}

    override val method: Method
        get() {
            when(this) {
                is getCurrentUser -> return Method.GET
                is getContacts -> return Method.GET
                is getPhotos -> return Method.GET
                is getPosts -> return Method.GET
                is getUser -> return Method.GET
                is updateCurrentUser -> return Method.PATCH
            }
        }

    override val path: String
        get() {
            when (this) {
                is getCurrentUser -> return "user"
                is getUser -> return "users/$userGuid"
                is getContacts -> return "users/$userGuid/contacts"
                is getPhotos -> return "users/$userGuid/photos"
                is getPosts -> return "users/$userGuid/posts"
                is updateCurrentUser -> return "user?access_token=${this.settings.accessToken}"
            }
        }

    override val params:List<Pair<String,Any?>>?
        get() = listOf("access_token" to this.settings.accessToken)

    override val headers: Map<String, String>?
        get() = mapOf("Content-Type" to "application/json")

    override val body: String?
        get() {
            if(this is updateCurrentUser) {
                return Gson().toJson(user)
            } else {
                return null
            }
        }
}