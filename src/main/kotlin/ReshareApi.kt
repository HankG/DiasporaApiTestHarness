package hankg.diaspora.api

import com.github.kittinunf.fuel.core.Method
import com.github.kittinunf.fuel.util.FuelRouting

sealed class ReshareApi(val postGuid:String, val settings:Settings) : FuelRouting {
    override val basePath = settings.baseUrl

    class list(postGuid:String, settings:Settings): ReshareApi(postGuid, settings) {}
    class reshare(postGuid:String, settings:Settings): ReshareApi(postGuid, settings) {}

    override val method: Method
        get() = when(this) {
            is list -> Method.GET
            is reshare -> Method.POST
        }

    override val path: String
        get() = "posts/${this.postGuid}/reshares?access_token=${this.settings.accessToken}"

    override val params:List<Pair<String,Any?>>?
        get() = null

    override val headers: Map<String, String>?
        get() = mapOf("Content-Type" to "application/json")

    override val body: String?
        get() = null
}