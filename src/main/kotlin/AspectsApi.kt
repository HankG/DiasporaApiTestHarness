package hankg.diaspora.api

import com.github.kittinunf.fuel.core.Method
import com.github.kittinunf.fuel.util.FuelRouting
import com.google.gson.Gson

sealed class AspectsApi(val settings:Settings = globalSettings) : FuelRouting {
    override val basePath = settings.baseUrl

    class addAspect(val aspect:Aspect, settings:Settings): AspectsApi(settings) {}
    class editAspect(val aspect:Aspect, settings:Settings): AspectsApi(settings) {}
    class deleteAspect(val aspectId:Int, settings:Settings): AspectsApi(settings) {}
    class getInformation(val aspectId:Int, settings:Settings): AspectsApi(settings) {}
    class listAspects(settings:Settings): AspectsApi(settings) {}


    override val method: Method
        get() {
            when(this) {
                is addAspect -> return Method.POST
                is editAspect -> return Method.PATCH
                is deleteAspect -> return Method.DELETE
                else -> return Method.GET
            }
        }

    override val path: String
        get() {
            when(this) {
                is addAspect -> return "aspects?access_token=${this.settings.accessToken}"
                is editAspect -> return "aspects/${this.aspect.id}?access_token=${this.settings.accessToken}"
                is deleteAspect -> return "aspects/${this.aspectId}"
                is getInformation -> return "aspects/${this.aspectId}"
                is listAspects -> return "aspects"
            }
        }

    override val params:List<Pair<String,Any?>>?
        get() = listOf("access_token" to this.settings.accessToken)

    override val headers: Map<String, String>?
        get() = mapOf("Content-Type" to "application/json")

    override val body: String?
        get() {
            val bodyParams = mutableMapOf<String, Any?>()
            when(this) {
                is addAspect -> this.aspect.fillParamList(bodyParams, false)
                is editAspect -> this.aspect.fillParamList(bodyParams,true)
                else -> {}
            }
            return Gson().toJson(bodyParams)
        }
}