package hankg.diaspora.api

import com.github.kittinunf.fuel.core.Method
import com.github.kittinunf.fuel.util.FuelRouting

sealed class StreamsApi(val settings:Settings) : FuelRouting {
    override val basePath = settings.baseUrl

    class getAspectsStream(val aspects:List<Aspect> = listOf<Aspect>(), settings:Settings): StreamsApi(settings) {}
    class getActivityStream(settings:Settings): StreamsApi(settings) {}
    class getCommentedStream(settings:Settings): StreamsApi(settings) {}
    class getLikedStream(settings:Settings): StreamsApi(settings) {}
    class getMainStream(settings:Settings): StreamsApi(settings) {}
    class getMentionsStream(settings:Settings): StreamsApi(settings) {}
    class getTagsStream(settings:Settings): StreamsApi(settings) {}

    override val method: Method
        get() = Method.GET

    override val path: String
        get() = when(this) {
            is getAspectsStream -> "streams/aspects"
            is getActivityStream -> "streams/activity"
            is getCommentedStream -> "streams/commented"
            is getLikedStream -> "streams/liked"
            is getMainStream -> "streams/main"
            is getMentionsStream -> "streams/mentions"
            is getTagsStream -> "streams/tags"
        }

    override val params:List<Pair<String,Any?>>?
        get() = when(this){
            is getAspectsStream -> {
                if(this.aspects.isEmpty()) {
                    listOf("access_token" to this.settings.accessToken)
                } else {
                    listOf("access_token" to this.settings.accessToken, "aspect_ids" to this.aspects.map {it.id})
                }
            }
            else -> listOf("access_token" to this.settings.accessToken)
        }

    override val headers: Map<String, String>?
        get() = mapOf("Content-Type" to "application/json")

    override val body: String?
        get() = null
}