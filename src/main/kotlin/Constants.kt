package hankg.diaspora.api;

import java.io.File


private val defaultAcPath = "/diaspora_ac.txt"

val globalSettings = Settings()

data class Settings(var accessToken:String = File(defaultAcPath).readLines().last()) {
    private val apiBase = "api/v1"

    var server:String = "http://localhost:3000"
        set(value) {
            field = value
            this.baseUrl = "$server/$apiBase"
        }

    var baseUrl:String = "http://localhost:3000/api/v1"
        private set(value) {
            field = value
        }
}