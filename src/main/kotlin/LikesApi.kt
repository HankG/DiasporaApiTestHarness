package hankg.diaspora.api

import com.github.kittinunf.fuel.core.Method
import com.github.kittinunf.fuel.util.FuelRouting

sealed class LikesApi(val postGuid: String, val settings:Settings) : FuelRouting {
    override val basePath = settings.baseUrl

    class showLikes(postGuid: String, settings:Settings): LikesApi(postGuid, settings) {}
    class addLike(postGuid: String, settings:Settings): LikesApi(postGuid, settings) {}
    class removeLike(postGuid: String, settings:Settings): LikesApi(postGuid, settings) {}

    override val method: Method
        get() {
            when(this) {
                is showLikes -> return Method.GET
                is addLike -> return Method.POST
                is removeLike -> return Method.DELETE
            }
        }

    override val path: String
        get() = "posts/${this.postGuid}/likes?access_token=${this.settings.accessToken}"

    override val params:List<Pair<String,Any?>>?
        get() = null

    override val headers: Map<String, String>?
        get() = mapOf("Content-Type" to "application/json")

    override val body: String?
        get() = null
}