package hankg.diaspora.api

import com.github.kittinunf.fuel.core.Method
import com.github.kittinunf.fuel.util.FuelRouting
import com.google.gson.Gson

sealed class PostsApi(val settings:Settings) : FuelRouting {
    override val basePath = settings.baseUrl

    class getPost(val postGuid: String, settings:Settings): PostsApi(settings) {}
    class deletePost(val postGuid: String, settings:Settings): PostsApi(settings) {}
    class addPost(val post:Post, val aspects:List<Aspect> = listOf<Aspect>(), settings:Settings): PostsApi(settings) {}

    override val method: Method
        get() {
            when(this) {
                is getPost -> return Method.GET
                is addPost -> return Method.POST
                is deletePost -> return Method.DELETE
            }
        }

    override val path: String
        get() {
            when(this) {
                is getPost -> return "posts/${this.postGuid}"
                is deletePost -> return "posts/${this.postGuid}"
                is addPost -> return "posts?access_token=${this.settings.accessToken}"
            }
        }

    override val params:List<Pair<String,Any?>>?
        get() = when(this) {
            is deletePost -> listOf("access_token" to this.settings.accessToken)
            is getPost -> listOf("access_token" to this.settings.accessToken)
            is addPost -> listOf("access_token" to this.settings.accessToken)
        }

    override val headers: Map<String, String>?
        get() = mapOf("Content-Type" to "application/json")

    override val body: String?
        get() = when(this) {
            is getPost -> null
            is deletePost -> null
            is addPost -> {
                val bodyParams = mutableMapOf<String, Any?>()
                this.post.fillNewPostRequestData(bodyParams, this.aspects)
                if(!this.post.photos.isEmpty()) {
                    bodyParams["photos"] = this.post.photos.map { it.guid }.toList()
                }
                Gson().toJson(bodyParams)
            }
        }
}