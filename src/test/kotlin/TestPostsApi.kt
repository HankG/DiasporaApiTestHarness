package hankg.diaspora.api

import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.DataPart
import junit.framework.TestCase
import org.junit.Test
import java.io.File
import java.lang.IllegalArgumentException
import java.time.LocalDateTime


class TestPostsApi {
    val postGuid = "2864bbf0bf650136b2b9080027f3b408"
    val fullPostGuid = "de738940c0c901361ba5080027f3b408"
    val resharePostGuid = "12e5f6a0b7660136e8a1080027f3b408"

    @Test
    fun testGetPost() {
        val (_, response, result) = Fuel.request(PostsApi.getPost(postGuid, globalSettings))
                .responseObject(Post.Deserializer())
        TestCase.assertEquals(response.statusCode, 200)
        println(result)
    }

    @Test
    fun testGetFullPost() {
        val (_, response, result) = Fuel.request(PostsApi.getPost(fullPostGuid, globalSettings))
                .responseObject(Post.Deserializer())
        TestCase.assertEquals(response.statusCode, 200)
        println(result)
    }

    @Test
    fun testGetResharePost() {
        val (_, response, result) = Fuel.request(PostsApi.getPost(resharePostGuid, globalSettings))
                .responseObject(Post.Deserializer())
        TestCase.assertEquals(response.statusCode, 200)
        println(result)
    }

    @Test
    fun testAddSimpleTextPublicPost() {
        val text = "New post at ${LocalDateTime.now()}"
        val post = Post(body = text, public = true)
        val (_, response, result) = Fuel.request(PostsApi.addPost(post, settings = globalSettings))
                .responseObject(Post.Deserializer())
        TestCase.assertEquals(response.statusCode, 200)
        println(result)
    }

    @Test
    fun testAddSimplePrivateTextPublicPost() {
        val (_, _, aspectResult) = Fuel.request(AspectsApi.listAspects(globalSettings)).responseObject(Aspect.ArrayDeserializer())
        val familyAspect = aspectResult.get().find { it.name == "Family"}
        if (familyAspect == null) throw IllegalArgumentException("Need to have a family aspect")
        val aspects = listOf<Aspect>(familyAspect)

        val text = "New post at ${LocalDateTime.now()}"
        val post = Post(body = text, public = false)
        val (_, response, result) = Fuel.request(PostsApi.addPost(post, aspects, settings = globalSettings))
                .responseObject(Post.Deserializer())
        TestCase.assertEquals(response.statusCode, 200)
        println(result)
    }

    @Test
    fun testDeletePost() {
        val text = "New post at ${LocalDateTime.now()}"
        val postRequest = Post(body = text)
        val (_, response, result) = Fuel.request(PostsApi.addPost(postRequest, settings = globalSettings))
                .responseObject(Post.Deserializer())
        TestCase.assertEquals(response.statusCode, 200)
        val createdPost = result.get()
        val (_, getResponse, responseFromGet) = Fuel.request(PostsApi.getPost(createdPost.guid, settings = globalSettings))
                .responseObject(Post.Deserializer())
        TestCase.assertEquals(getResponse.statusCode, 200)
        TestCase.assertEquals(createdPost.guid, responseFromGet.get().guid)

        val (_, deleteResponse, _) = Fuel.request(PostsApi.deletePost(createdPost.guid, settings = globalSettings)).response()
        TestCase.assertEquals(deleteResponse.statusCode, 204)
        val (_, getDeletedResponse, _) = Fuel.request(PostsApi.getPost(createdPost.guid, settings = globalSettings))
                .responseObject(Post.Deserializer())
        TestCase.assertEquals(getDeletedResponse.statusCode, 404)

    }


    @Test
    fun testPostWithPollAndLocationPost() {
        val text = "I'm visiting my grandma at Shady Pines.  Anyone heard of it? ${LocalDateTime.now()}"
        val poll = Poll(question = "Have you heard of it?", poll_answers = listOf<Poll.Answer>(Poll.Answer(answer = "yes"), Poll.Answer(answer = "no"), Poll.Answer(answer = "maybe")))
        val location = Location("Shady Pines", 25.7548, -80.1785)
        val post = Post(body = text, location = location, poll = poll)
        val (_, response, result) = Fuel.request(PostsApi.addPost(post,  settings = globalSettings))
                .responseObject(Post.Deserializer())
        println(result)
        TestCase.assertEquals(response.statusCode, 200)
    }

    @Test
    fun testPostWithPhoto() {
        val uploader = PhotosApi.upload(pending = true, profilePic = true, settings = globalSettings)
        val (_, presponse, presult) = Fuel.upload(uploader.fullPath, uploader.method, uploader.params)
                .dataParts { _, _ ->
                    listOf(
                            DataPart(File("/tmp/MediumAlarmclock.jpeg"), name = "image", type="image/jpg")
                    )
                }.responseObject(Photo.Deserializer())
        TestCase.assertEquals(presponse.statusCode, 200)
        val photo = presult.get()
        println("New photo for post: ${photo.guid}")
        val photos = listOf<Photo>(photo)
        val post = Post(body = "A post with a photo at ${LocalDateTime.now()}", photos = photos)
        val (_, response, result) = Fuel.request(PostsApi.addPost(post, settings = globalSettings))
                .responseObject(Post.Deserializer())
        println(result)
        TestCase.assertEquals(response.statusCode, 200)
        TestCase.assertEquals(result.get().photos.size, 1)
    }

}
