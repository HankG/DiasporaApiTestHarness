import hankg.diaspora.api.PageDataUrls
import junit.framework.TestCase
import org.junit.Test


class TestPageDataUrl {

    val firstUrl = "http://localhost/1"
    val firstCursor = "<$firstUrl> ; rel=\"first\""
    val lastUrl = "http://localhost/100"
    val lastCursor = "<$lastUrl> ; rel=\"last\""
    val nextUrl = "http://localhost/4"
    val nextCursor = "<$nextUrl> ; rel=\"next\""
    val previousUrl = "http://localhost/3"
    val previousCursor = "<$previousUrl> ; rel=\"previous\""


    @Test
    fun testFullHeaderOneString()
    {
        val headers = mapOf("field1" to listOf("string1, string2"),
                "Link" to listOf("$nextCursor,$previousCursor,$firstCursor,$lastCursor")
        )

        val pageData = PageDataUrls.fromHeaderLinkField(headers)
        TestCase.assertEquals(firstUrl,pageData.first)
        TestCase.assertEquals(lastUrl,pageData.last)
        TestCase.assertEquals(previousUrl,pageData.previous)
        TestCase.assertEquals(nextUrl,pageData.next)
    }

    @Test
    fun testFullHeaderManyStrings()
    {
        val headers = mapOf("field1" to listOf("string1, string2"),
                "Link" to listOf(nextCursor, previousCursor, firstCursor,lastCursor)
        )

        val pageData = PageDataUrls.fromHeaderLinkField(headers)
        TestCase.assertEquals(firstUrl,pageData.first)
        TestCase.assertEquals(lastUrl,pageData.last)
        TestCase.assertEquals(previousUrl,pageData.previous)
        TestCase.assertEquals(nextUrl,pageData.next)
    }

    @Test
    fun testFullHeaderWithoutLinkField()
    {
        val headers = mapOf("field1" to listOf("string1, string2"),
                "field2" to listOf("$nextCursor,$previousCursor,$firstCursor,$lastCursor")
        )

        val pageData = PageDataUrls.fromHeaderLinkField(headers)
        TestCase.assertTrue(pageData.first.isEmpty())
        TestCase.assertTrue(pageData.last.isEmpty())
        TestCase.assertTrue(pageData.next.isEmpty())
        TestCase.assertTrue(pageData.previous.isEmpty())
    }

    @Test
    fun testEmptyHeader()
    {
        val headers = mapOf<String, Collection<String>>()
        val pageData = PageDataUrls.fromHeaderLinkField(headers)
        TestCase.assertTrue(pageData.first.isEmpty())
        TestCase.assertTrue(pageData.last.isEmpty())
        TestCase.assertTrue(pageData.next.isEmpty())
        TestCase.assertTrue(pageData.previous.isEmpty())
    }

    @Test
    fun testEmptyString()
    {
        val pageData = PageDataUrls.fromHeaderLinkField("")
        TestCase.assertTrue(pageData.first.isEmpty())
        TestCase.assertTrue(pageData.last.isEmpty())
        TestCase.assertTrue(pageData.next.isEmpty())
        TestCase.assertTrue(pageData.previous.isEmpty())
    }

    @Test
    fun testAllFields()
    {
        val headerLinkField = "$nextCursor,$previousCursor,$firstCursor,$lastCursor"
        val pageData = PageDataUrls.fromHeaderLinkField(headerLinkField)
        TestCase.assertEquals(firstUrl,pageData.first)
        TestCase.assertEquals(lastUrl,pageData.last)
        TestCase.assertEquals(previousUrl,pageData.previous)
        TestCase.assertEquals(nextUrl,pageData.next)
    }

    @Test
    fun testOnlyPrevious()
    {
        val pageData = PageDataUrls.fromHeaderLinkField(previousCursor)
        TestCase.assertTrue(pageData.first.isEmpty())
        TestCase.assertTrue(pageData.last.isEmpty())
        TestCase.assertTrue(pageData.next.isEmpty())
        TestCase.assertEquals(previousUrl,pageData.previous)
    }

    @Test
    fun testOnlyNext()
    {
        val pageData = PageDataUrls.fromHeaderLinkField(nextCursor)
        TestCase.assertTrue(pageData.first.isEmpty())
        TestCase.assertTrue(pageData.last.isEmpty())
        TestCase.assertTrue(pageData.previous.isEmpty())
        TestCase.assertEquals(nextUrl,pageData.next)
    }

    @Test
    fun testExtraFields()
    {
        val extra1 = "<http://somewhere> ; rel=\"extra1\""
        val extra2 = "<http://somewhere> ; rel=\"extra2\""
        val extra3 = "<http://somewhere> ; rel=\"extra3\""
        val headerLinkField = "$extra1,$nextCursor,$previousCursor,$extra2,$firstCursor,$lastCursor,$extra3"
        val pageData = PageDataUrls.fromHeaderLinkField(headerLinkField)
        TestCase.assertEquals(firstUrl,pageData.first)
        TestCase.assertEquals(lastUrl,pageData.last)
        TestCase.assertEquals(previousUrl,pageData.previous)
        TestCase.assertEquals(nextUrl,pageData.next)
    }

    @Test
    fun testMalformedExtra()
    {
        val extra1 = "this is really off the format for a cursor"
        val headerLinkField = "$extra1,$nextCursor,$previousCursor,$firstCursor,$lastCursor"
        val pageData = PageDataUrls.fromHeaderLinkField(headerLinkField)
        TestCase.assertEquals(firstUrl,pageData.first)
        TestCase.assertEquals(lastUrl,pageData.last)
        TestCase.assertEquals(previousUrl,pageData.previous)
        TestCase.assertEquals(nextUrl,pageData.next)
    }

    @Test
    fun testStringNewlinesTabs()
    {
        val headerLinkField = "\n$nextCursor,\n\t $previousCursor,\n\t $firstCursor,\n\t$lastCursor\n\n"
        val pageData = PageDataUrls.fromHeaderLinkField(headerLinkField)
        TestCase.assertEquals(firstUrl,pageData.first)
        TestCase.assertEquals(lastUrl,pageData.last)
        TestCase.assertEquals(previousUrl,pageData.previous)
        TestCase.assertEquals(nextUrl,pageData.next)
    }

    @Test
    fun testBareUrl()
    {
        val previousBareCursor = "$previousUrl; rel=\"previous\""
        val pageData = PageDataUrls.fromHeaderLinkField(previousBareCursor)
        TestCase.assertEquals(previousUrl,pageData.previous)
    }

    @Test
    fun testBareCursorName()
    {
        val previousBareCursor = "<$previousUrl>; rel=previous"
        val pageData = PageDataUrls.fromHeaderLinkField(previousBareCursor)
        TestCase.assertEquals(previousUrl,pageData.previous)
    }

}
