package hankg.diaspora.api

import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.DataPart
import junit.framework.TestCase
import org.junit.Test
import java.io.File
import java.lang.IllegalArgumentException
import java.net.URI
import java.nio.file.Paths
import java.util.*

class TestPhotosApi {
    @Test
    fun testListUserPhotos() {
        val (_, response, result) = Fuel.request(PhotosApi.list(globalSettings)).responseObject(Photo.ArrayDeserializer())
        TestCase.assertEquals(response.statusCode, 200)
        val photos = result.get()
        photos.forEach { println(it.sizes.medium)}
    }

    @Test
    fun testGetPhotoDetails() {
        val myPhotoGuid = "d0786b50bf640136b2b9080027f3b408"
        val (_, response, result) = Fuel.request(PhotosApi.info(myPhotoGuid, globalSettings)).responseObject(Photo.Deserializer())
        TestCase.assertEquals(response.statusCode, 200)
        println(result.get())

        val otherPhotoGuid = "11734570d2f501366b0d080027f3b408"
        val (_, otherrsponse, otherresult) = Fuel.request(PhotosApi.info(otherPhotoGuid, globalSettings)).responseObject(Photo.Deserializer())
        TestCase.assertEquals(otherrsponse.statusCode, 200)
        println(otherresult.get())
    }

    @Test
    fun testDeletePhoto() {
        val (_, response, _) = Fuel.request(PhotosApi.delete(UUID.randomUUID().toString(), globalSettings)).response()
        TestCase.assertEquals(response.statusCode, 404)
    }

    @Test
    fun testUploadAndDeletePhoto() {
        val uploader = PhotosApi.upload(pending = true, profilePic = true, settings = globalSettings)
        val (_, response, result) = Fuel.upload(uploader.fullPath, uploader.method, uploader.params)
                .dataParts { _, _ ->
                    listOf(
                            DataPart(File("/tmp/MediumAlarmclock.jpeg"), name = "image", type="image/jpg")
                    )
                }.responseObject(Photo.Deserializer())
        println(result.get())
        TestCase.assertEquals(response.statusCode, 200)
        val (_, dr, _) = Fuel.request(PhotosApi.delete(result.get().guid, globalSettings)).response()
        TestCase.assertEquals(dr.statusCode, 204)
    }


    @Test
    fun testPrivatePhotoUpload() {
        val (_, _, aspectResult) = Fuel.request(AspectsApi.listAspects(globalSettings)).responseObject(Aspect.ArrayDeserializer())
        val familyAspect = aspectResult.get().find { it.name == "Family"}
        if (familyAspect == null) throw IllegalArgumentException("Need to have a family aspect")
        val aspects = listOf<Aspect>(familyAspect)

        val uploader = PhotosApi.upload(pending = false, profilePic = false, aspects = aspects, settings = globalSettings)
        val (_, response, result) = Fuel.upload(uploader.fullPath, uploader.method, uploader.params)
                .dataParts { _, _ ->
                    listOf(
                            DataPart(File("/tmp/MediumAlarmclock.jpeg"), name = "image", type="image/jpg")
                    )
                }.responseObject(Photo.Deserializer())
        println(result.get())
        TestCase.assertEquals(response.statusCode, 200)
        val (_, dr, _) = Fuel.request(PhotosApi.delete(result.get().guid, globalSettings)).response()
        TestCase.assertEquals(dr.statusCode, 204)
    }
}