package hankg.diaspora.api

import com.github.kittinunf.fuel.Fuel
import junit.framework.TestCase
import org.junit.Test
import java.time.LocalDateTime

class TestInteractionsApi {

    val postGuid = "b5ea8e70dde00136599a080027f3b408"

    @Test
    fun testSubscribe() {
        val (_, response, _) = Fuel.request(PostInteractions.subscribe(postGuid, globalSettings))
                .responseObject(Comment.ArrayDeserializer())
        TestCase.assertEquals(response.statusCode, 204)
    }

    @Test
    fun testMute() {
        val (_, response, _) = Fuel.request(PostInteractions.mute(postGuid, globalSettings))
                .responseObject(Comment.ArrayDeserializer())
        TestCase.assertEquals(response.statusCode, 204)
    }

    @Test
    fun testHideToggle() {
        val (_, response, _) = Fuel.request(PostInteractions.toggleHide(postGuid, globalSettings))
                .responseObject(Comment.ArrayDeserializer())
        TestCase.assertEquals(response.statusCode, 204)
    }

    @Test
    fun testReportPost() {
        val reason = "Reason to report ${LocalDateTime.now()}"
        val (_, response, _) = Fuel.request(PostInteractions.report(postGuid, reason,globalSettings))
                .responseObject(Comment.ArrayDeserializer())
        TestCase.assertEquals(response.statusCode, 204)
    }

    @Test
    fun testVoteOnPost() {
        val text = "I'm visiting my grandma at Shady Pines.  Anyone heard of it? ${LocalDateTime.now()}"
        val poll = Poll(question = "Have you heard of it?", poll_answers = listOf<Poll.Answer>(Poll.Answer(answer = "yes"), Poll.Answer(answer = "no"), Poll.Answer(answer = "maybe")))
        val location = Location("Shady Pines", 25.7548, -80.1785)
        val post = Post(body = text, location = location, poll = poll)
        val (_, _, result) = Fuel.request(PostsApi.addPost(post,  settings = globalSettings))
                .responseObject(Post.Deserializer())
        val createdPost= result.get()
        println(createdPost.poll)
        val firstAnswer = createdPost.poll.poll_answers.first()
        val (_, voteResponse, _) = Fuel.request(PostInteractions.vote(createdPost.guid, firstAnswer.id ,globalSettings))
                .responseObject(Comment.ArrayDeserializer())
        TestCase.assertEquals(voteResponse.statusCode, 204)
        val (_,_,updatedPost) = Fuel.request(PostsApi.getPost(createdPost.guid, globalSettings)).responseObject(Post.Deserializer())
        println(updatedPost.get().poll)
        Fuel.request(PostsApi.deletePost(createdPost.guid, globalSettings)).responseObject(Post.Deserializer())
    }

}
