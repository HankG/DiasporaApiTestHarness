package hankg.diaspora.api

import com.github.kittinunf.fuel.Fuel
import junit.framework.TestCase
import org.junit.Test

class TestAspectsApi {

    @Test
    fun testListAspects() {
        val (_, response, result) = Fuel.request(AspectsApi.listAspects(globalSettings))
                .responseObject(Aspect.ArrayDeserializer())
        result.get().forEach { println("${it.name}(${it.id})") }
        TestCase.assertEquals(response.statusCode, 200)
    }

    @Test
    fun testGetAspects() {
        val (_, response, result) = Fuel.request(AspectsApi.listAspects(globalSettings))
                .responseObject(Aspect.ArrayDeserializer())
        TestCase.assertEquals(response.statusCode, 200)
        val firstAspect = result.get().first()
        val (_, gr, aspectDetails) = Fuel.request(AspectsApi.getInformation(firstAspect.id, globalSettings))
                .responseObject(Aspect.Deserializer())
        TestCase.assertEquals(gr.statusCode, 200)
        println(aspectDetails.get())
    }

    @Test
    fun testCreateAndDeleteAspect() {
        val newAspect = Aspect(name="NA${System.currentTimeMillis()}")

        val (_, firstPull, firstResult) = Fuel.request(AspectsApi.listAspects(globalSettings))
                .responseObject(Aspect.ArrayDeserializer())
        println("Initial Value" + firstResult.get().map {it.name})
        TestCase.assertEquals(firstPull.statusCode, 200)

        val (_, cr, createResult) = Fuel.request(AspectsApi.addAspect(newAspect, globalSettings))
                .responseObject(Aspect.Deserializer())
        println(createResult)
        TestCase.assertEquals(cr.statusCode, 200)

        val (_, gr, gresult) = Fuel.request(AspectsApi.listAspects(globalSettings))
                .responseObject(Aspect.ArrayDeserializer())
        println("After add: " + gresult.get().map {it.name})
        TestCase.assertEquals(gr.statusCode, 200)
        TestCase.assertEquals(firstResult.get().size + 1, gresult.get().size)

        val (_, dr, _) = Fuel.request(AspectsApi.deleteAspect(createResult.get().id, globalSettings))
                .response()
        TestCase.assertEquals(dr.statusCode, 204)

        val (_, finalPull, finalResult) = Fuel.request(AspectsApi.listAspects(globalSettings))
                .responseObject(Aspect.ArrayDeserializer())
        println("Final value: " + finalResult.get().map {it.name})
        TestCase.assertEquals(finalPull.statusCode, 200)
    }

    @Test
    fun testUpdate() {
        val (_, firstPull, firstResult) = Fuel.request(AspectsApi.listAspects(globalSettings))
                .responseObject(Aspect.ArrayDeserializer())
        println("Initial Value" + firstResult.get().map {it.name})
        TestCase.assertEquals(firstPull.statusCode, 200)

        val newAspect = Aspect(name="NA${System.currentTimeMillis()}")
        val (_, cr, createResult) = Fuel.request(AspectsApi.addAspect(newAspect, globalSettings))
                .responseObject(Aspect.Deserializer())
        println(createResult.get())
        TestCase.assertEquals(cr.statusCode, 200)

        val (_, gr, gresult) = Fuel.request(AspectsApi.listAspects(globalSettings))
                .responseObject(Aspect.ArrayDeserializer())
        println("After add: " + gresult.get().map {it.name})
        TestCase.assertEquals(gr.statusCode, 200)
        TestCase.assertEquals(firstResult.get().size + 1, gresult.get().size)

        val updateAspect = createResult.get().copy(name = "UA${System.currentTimeMillis()}", order = 0,
                chat_enabled = !newAspect.chat_enabled)
        val (_, ur, updateResult) = Fuel.request(AspectsApi.editAspect(updateAspect, globalSettings))
                .responseObject(Aspect.Deserializer())
        println(updateResult.get())
        TestCase.assertEquals(ur.statusCode, 200)
        TestCase.assertEquals(updateAspect.name, updateResult.get().name)
        TestCase.assertEquals(updateAspect.chat_enabled, updateResult.get().chat_enabled)
        TestCase.assertEquals(updateAspect.order, updateResult.get().order)

        val (_, gr2, gresult2) = Fuel.request(AspectsApi.listAspects(globalSettings))
                .responseObject(Aspect.ArrayDeserializer())
        println("After update: " + gresult2.get().map {it.name})
        TestCase.assertEquals(gr2.statusCode, 200)

        val (_, dr, _) = Fuel.request(AspectsApi.deleteAspect(createResult.get().id, globalSettings))
                .response()
        TestCase.assertEquals(dr.statusCode, 204)
    }
}