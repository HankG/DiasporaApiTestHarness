package hankg.diaspora.api

import com.github.kittinunf.fuel.Fuel
import junit.framework.TestCase
import org.junit.Test

class TestSearchApi {
    @Test
    fun testSearchUsersByTag() {
        val (_, response, result) = Fuel.request(SearchApi.searchUsersByTag("newhere", globalSettings)).responseObject(Person.ArrayDeserializer())
        result.get().forEach { println(it.name) }
        val pageData = PageDataUrls.fromHeaderLinkField(response.headers)
        println("Previous: ${pageData.previous}")
        println("Next: ${pageData.next}")
        TestCase.assertEquals(response.statusCode, 200)
    }

    @Test
    fun testSearchUsersByname() {
        val (_, response, result) = Fuel.request(SearchApi.searchUsersByName("User", globalSettings)).responseObject(Person.ArrayDeserializer())
        result.get().forEach { println(it.name) }
        TestCase.assertEquals(response.statusCode, 200)
    }

    @Test
    fun testPostsByTag() {
        val (_, response, result) = Fuel.request(SearchApi.searchPosts("linux", globalSettings)).responseObject(Post.ArrayDeserializer())
        result.get().forEach { println(it.body) }
        println(PageDataUrls.fromHeaderLinkField(response.headers))
        TestCase.assertEquals(response.statusCode, 200)
    }

}