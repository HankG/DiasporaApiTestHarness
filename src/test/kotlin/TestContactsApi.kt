package hankg.diaspora.api

import com.github.kittinunf.fuel.Fuel
import junit.framework.TestCase
import org.junit.Before
import org.junit.Test

class TestContactsApi {

    var aspects = mutableListOf<Aspect>()

    @Before
    fun fillAspects() {
        val (_, _, result) = Fuel.request(AspectsApi.listAspects(globalSettings))
                .responseObject(Aspect.ArrayDeserializer())
        println("Aspects:" + result.get().map{it.name})
        aspects.addAll(result.get())
    }

    @Test
    fun testListContacts() {
        for(a in aspects) {
            val (_, response, result) = Fuel.request(ContactsApi.getContacts(a.id.toString(), globalSettings))
                    .responseObject(Person.ArrayDeserializer())
            TestCase.assertEquals(response.statusCode, 200)
            val contacts = result.get()
            print("${a.name} (ID=${a.id}) Members: ")
            if (contacts.isEmpty()) print("None") else println()
            contacts.forEach { println("${it.name} (${it.avatar})") }
            println(PageDataUrls.fromHeaderLinkField(response.headers))
            if (contacts.isNotEmpty()) println("=========================================")
        }
    }

    @Test
    fun testAddRemoveContact() {
        val user3Stub = Person(guid = "a0601b90ad4a013627ab080027f3b408")
        val (_, response) = Fuel.request(ContactsApi.addContact(aspects.first().id.toString(), user3Stub, globalSettings))
                .response()
        TestCase.assertEquals(response.statusCode, 204)

        val (_, _, gc1) = Fuel.request(ContactsApi.getContacts(aspects.first().id.toString(), globalSettings))
                .responseObject(Person.ArrayDeserializer())
        println(gc1.get().map {it.name})

        val (_, response2) = Fuel.request(ContactsApi.deleteContact(aspects.first().id.toString(), user3Stub, globalSettings))
                .response()
        TestCase.assertEquals(response2.statusCode, 204)

        val (_, _, gc2) = Fuel.request(ContactsApi.getContacts(aspects.first().id.toString(), globalSettings))
                .responseObject(Person.ArrayDeserializer())
        println(gc2.get().map {it.name})
    }

}