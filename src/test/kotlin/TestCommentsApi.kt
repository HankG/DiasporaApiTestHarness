package hankg.diaspora.api

import com.github.kittinunf.fuel.Fuel
import junit.framework.TestCase
import org.junit.Test
import java.time.LocalDateTime

class TestCommentsApi {

    val postGuid = "b5ea8e70dde00136599a080027f3b408"

    @Test
    fun testGetComments() {
        val (_, response, result) = Fuel.request(CommentsApi.getPostComments(postGuid, globalSettings))
                .responseObject(Comment.ArrayDeserializer())
        println(PageDataUrls.fromHeaderLinkField(response.headers))
        println(result.get())
        TestCase.assertEquals(response.statusCode, 200)
    }

    @Test
    fun testAddAndDeleteComment() {
        val commentText = "New comment through the API at ${LocalDateTime.now()}"
        val (_, response, result) = Fuel.request(CommentsApi.addComment(postGuid, commentText, globalSettings))
                .responseObject(Comment.Deserializer())
        val comment = result.component1()
        println(comment)
        TestCase.assertEquals(response.statusCode, 201)

        val (_, responsed, _) = Fuel.request(CommentsApi.deleteComment(postGuid, comment?.guid?:"", globalSettings)).response()
        TestCase.assertEquals(responsed.statusCode, 204)
    }

    @Test
    fun testAddAndReportComment() {
        val commentText = "New comment to report on through the API at ${LocalDateTime.now()}"
        val (_, response, result) = Fuel.request(CommentsApi.addComment(postGuid, commentText, globalSettings))
                .responseObject(Comment.Deserializer())
        val comment = result.component1()
        println(comment)
        TestCase.assertEquals(response.statusCode, 201)

        val reason = "Automated post should be flagged in test"
        val (_, responser, _) = Fuel.request(CommentsApi.reportComment(postGuid, comment?.guid?:"", reason, globalSettings)).response()
        TestCase.assertEquals(responser.statusCode, 204)
    }

}
