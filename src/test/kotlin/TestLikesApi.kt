package hankg.diaspora.api

import com.github.kittinunf.fuel.Fuel
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.fail
import org.junit.Test

class TestLikesApi {

    val postGuid = "b5ea8e70dde00136599a080027f3b408"

    @Test
    fun testShowLikes() {
        val (_, response, result) = Fuel.request(LikesApi.showLikes(postGuid, globalSettings)).responseObject(Like.ArrayDeserializer())
        assertEquals(response.statusCode, 200)
        println(result)
    }

    @Test
    fun testAddLike() {
        val (_, response, _) = Fuel.request(LikesApi.addLike(postGuid, globalSettings)).responseObject(Like.ArrayDeserializer())
        if(response.statusCode == 422) {
            val errorMessage = String(response.data)
            if(errorMessage == "Like already exists") {
                val (_, removeResponse, _) = Fuel.request(LikesApi.removeLike(postGuid, globalSettings)).responseObject(Like.ArrayDeserializer())
                assertEquals(removeResponse.statusCode, 204)
                val (_, addAgainResponse, _) = Fuel.request(LikesApi.addLike(postGuid, globalSettings)).responseObject(Like.ArrayDeserializer())
                assertEquals(addAgainResponse.statusCode, 204)
            } else {
                fail()
            }
        } else {
            assertEquals(response.statusCode, 204)
            val (_, removeResponse, _) = Fuel.request(LikesApi.removeLike(postGuid, globalSettings)).responseObject(Like.ArrayDeserializer())
            assertEquals(removeResponse.statusCode, 204)
        }

    }

    @Test
    fun testRemoveLike() {
        Fuel.request(LikesApi.addLike(postGuid, globalSettings)).response()
        val (_, response, _) = Fuel.request(LikesApi.removeLike(postGuid, globalSettings)).responseObject(Like.ArrayDeserializer())
        assertEquals(response.statusCode, 204)
    }

}

