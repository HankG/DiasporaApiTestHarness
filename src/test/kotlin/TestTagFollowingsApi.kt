package hankg.diaspora.api

import com.github.kittinunf.fuel.Fuel
import junit.framework.TestCase
import org.junit.Test

class TestTagFollowingsApi {

    @Test
    fun testGetFollowedTags() {
        val (_, response, result) = Fuel.request(TagFollowingsApi.getList(globalSettings))
                .responseObject(StringArrayDeserializer())
        println(result)
        TestCase.assertEquals(response.statusCode, 200)

    }

    @Test
    fun testAddRemoveFollowedTags() {
        val tagName = "NewTag${System.currentTimeMillis()}"
        val (_, ar, _) = Fuel.request(TagFollowingsApi.follow(tagName, globalSettings)).response()
        TestCase.assertEquals(ar.statusCode, 204)

        var tags = Fuel.request(TagFollowingsApi.getList(globalSettings))
                .responseObject(StringArrayDeserializer()).third
        println(tags.get())

        val (_, remove, _) = Fuel.request(TagFollowingsApi.unfollow(tagName, globalSettings)).response()
        TestCase.assertEquals(remove.statusCode, 204)

        tags = Fuel.request(TagFollowingsApi.getList(globalSettings))
                .responseObject(StringArrayDeserializer()).third
        println(tags.get())
    }

}