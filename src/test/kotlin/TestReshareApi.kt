package hankg.diaspora.api

import com.github.kittinunf.fuel.Fuel
import junit.framework.TestCase
import org.junit.Test

class TestReshareApi {

    val postGuid = "6b8eb090dd6e01365990080027f3b408"

    @Test
    fun testListOfReshares() {
        val (_, response, result) = Fuel.request(ReshareApi.list(postGuid, globalSettings))
                .responseObject(ReshareData.ArrayDeserializer())
        println(result.get())
        TestCase.assertEquals(response.statusCode, 200)
    }

    @Test
    fun testReshare() {
        val (_, response, result) = Fuel.request(ReshareApi.reshare(postGuid, globalSettings))
                .responseObject(ReshareData.Deserializer())
        println(result.get())
        TestCase.assertEquals(response.statusCode, 200)
        Fuel.request(PostsApi.deletePost(result.get().guid, globalSettings)).response()
    }

}