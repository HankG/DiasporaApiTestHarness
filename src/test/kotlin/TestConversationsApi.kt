package hankg.diaspora.api

import com.github.kittinunf.fuel.Fuel
import junit.framework.TestCase
import org.junit.Test
import java.time.LocalDateTime
import java.time.ZonedDateTime

class TestConversationsApi {
    val user1 = Person("6bdb9610ad4a013627a9080027f3b408")
    val user2 = Person("ac666a00ad4a013627ab080027f3b408")


    @Test
    fun testAddAndGetConversation() {

        val subject = "New Conversation at ${LocalDateTime.now()}"
        val text = "Hello from User1 at ${LocalDateTime.now()}"
        val recipients = listOf(user2)
        val newConversation = ConversationRequest(subject, text, recipients)
        val (request, response, result) = Fuel.request(ConversationsApi.addConversation(newConversation, globalSettings))
                .responseObject(Conversation.Deserializer())
        println(request)
        println("Result = ${result}")
        TestCase.assertEquals(response.statusCode, 201)

        val newConvoResult = result.get()

        val (_, response2, pulledConvo) = Fuel.request(ConversationsApi.getConversation(newConvoResult.guid, globalSettings))
                .responseObject(Conversation.Deserializer())
        println("PulledConvo: ${pulledConvo}")
        TestCase.assertEquals(response2.statusCode, 200)
    }

    @Test
    fun testGetConversations() {
        val (_, responseAll, resultAll) = Fuel.request(ConversationsApi.list(settings = globalSettings))
                .responseObject(Conversation.ArrayDeserializer())
        println("# Conversations: ${resultAll.get().size}")
        TestCase.assertEquals(responseAll.statusCode, 200)

        val (_, responseUnread, resultUnread) = Fuel.request(ConversationsApi.list(unreadOnly = true, settings = globalSettings))
                .responseObject(Conversation.ArrayDeserializer())
        println("# Conversations Unread: ${resultUnread.get().size}")
        TestCase.assertEquals(responseUnread.statusCode, 200)

        val (_, responseUnread2, resultUnread2) = Fuel.request(ConversationsApi.list(unreadOnly = false, settings = globalSettings))
                .responseObject(Conversation.ArrayDeserializer())
        println("# Conversations All: ${resultUnread2.get().size}")
        TestCase.assertEquals(responseUnread2.statusCode, 200)

        val timeFilter = ZonedDateTime.parse("2018-10-25T05:00:00.000Z")
        val (_, tresponse1, tr1) = Fuel.request(ConversationsApi.list(unreadOnly = false,
                updatedAfterOnly = true, afterTime = timeFilter, settings = globalSettings))
                .responseObject(Conversation.ArrayDeserializer())
        println("# Conversations All after ${timeFilter}: ${tr1.get().size}")
        TestCase.assertEquals(tresponse1.statusCode, 200)

        val (_, tresponse2, tr2) = Fuel.request(ConversationsApi.list(unreadOnly = true,
                updatedAfterOnly = true, afterTime = timeFilter, settings = globalSettings))
                .responseObject(Conversation.ArrayDeserializer())
        println("# Conversations Unread after ${timeFilter}: ${tr2.get().size}")
        TestCase.assertEquals(tresponse2.statusCode, 200)
        tr2.get().forEach { println(it) }
    }

    @Test
    fun testIgnoreAndHide() {
        val (_, _, resultAll) = Fuel.request(ConversationsApi.list(settings = globalSettings))
                .responseObject(Conversation.ArrayDeserializer())
        val originalSize = resultAll.get().size

        val subject = "New Conversation to hide at ${LocalDateTime.now()}"
        val text = "Hello from User1 (but will hide this) at ${LocalDateTime.now()}"
        val recipients = listOf(user2)
        val newConversation = ConversationRequest(subject, text, recipients)
        val (_, _, result) = Fuel.request(ConversationsApi.addConversation(newConversation, globalSettings))
                .responseObject(Conversation.Deserializer())
        val conversation = result.get()

        val (_, _, newResult) = Fuel.request(ConversationsApi.list(settings = globalSettings))
                .responseObject(Conversation.ArrayDeserializer())
        val newSize = newResult.get().size
        TestCase.assertEquals(originalSize + 1, newSize)

        val (_, hideResponse, _) = Fuel.request(ConversationsApi.ignoreAndHide(conversation.guid, globalSettings))
                .response()
        TestCase.assertEquals(hideResponse.statusCode, 204)

        val (_, _, finalResult) = Fuel.request(ConversationsApi.list(settings = globalSettings))
                .responseObject(Conversation.ArrayDeserializer())
        val finalSize = finalResult.get().size
        print("Final Conversations: ${finalResult.get()}")
        TestCase.assertEquals(originalSize, finalSize)
    }

    @Test
    fun testGetMessages() {
        val (_, _, allConvoResults) = Fuel.request(ConversationsApi.list(settings = globalSettings))
                .responseObject(Conversation.ArrayDeserializer())
        val conversation = allConvoResults.get()[0]

        val (_, msgsResponse, msgResults) = Fuel.request(ConversationsApi.getMessages(conversation, globalSettings))
                .responseObject(ConversationMessage.ArrayDeserializer())
        val messages = msgResults.get()
        messages.forEach { println("${it.author.name}: ${it.body}") }
        TestCase.assertEquals(msgsResponse.statusCode, 200)

        val (_, _, convoAfterRead) = Fuel.request(ConversationsApi.getConversation(conversation.guid, settings = globalSettings))
                .responseObject(Conversation.Deserializer())
        println(convoAfterRead.get())
        TestCase.assertTrue(convoAfterRead.get().read)
    }

    @Test
    fun testAddMessages() {
        val (_, _, allConvoResults) = Fuel.request(ConversationsApi.list(settings = globalSettings))
                .responseObject(Conversation.ArrayDeserializer())
        val conversation = allConvoResults.get().last()

        val (_, msgsResponse1, msgResults1) = Fuel.request(ConversationsApi.getMessages(conversation, globalSettings))
                .responseObject(ConversationMessage.ArrayDeserializer())
        val originalMessages = msgResults1.get()
        println("$originalMessages")
        TestCase.assertEquals(msgsResponse1.statusCode, 200)


        val text = "New message at ${LocalDateTime.now()}"
        val (_, addMsgResponse, addMsgResult) = Fuel.request(ConversationsApi.addMessage(conversation, text, globalSettings))
                .responseObject(ConversationMessage.Deserializer())
        val newMessage = addMsgResult.get()
        println("New Message: $newMessage")
        TestCase.assertEquals(addMsgResponse.statusCode, 201)


        val (_, msgsResponse2, msgResults2) = Fuel.request(ConversationsApi.getMessages(conversation, globalSettings))
                .responseObject(ConversationMessage.ArrayDeserializer())
        val finalMessages = msgResults2.get()
        println("$finalMessages")
        TestCase.assertEquals(msgsResponse2.statusCode, 200)
        TestCase.assertEquals(originalMessages.size + 1, finalMessages.size)

    }

}