package hankg.diaspora.api

import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.httpGet
import junit.framework.TestCase
import org.junit.Test

class TestUsersApi {
    val user1Guid = "6bdb9610ad4a013627a9080027f3b408"
    val user2Guid = "ac666a00ad4a013627ab080027f3b408"
    val user3Guid = "a0601b90ad4a013627ab080027f3b408"

    @Test
    fun testGetCurrentUser() {
        val (_, response, result) = Fuel.request(UsersApi.getCurrentUser(globalSettings))
                .responseObject(User.Deserializer())
        println(result.get())
        TestCase.assertEquals(response.statusCode, 200)
    }

    @Test
    fun testUpdateCurrentUser() {
        val (_, _, currentUserResult) = Fuel.request(UsersApi.getCurrentUser(globalSettings))
                .responseObject(User.Deserializer())
        val originalUser = currentUserResult.get()
        val oldFirstName = originalUser.name
        val userUpdate = originalUser.copy(name = "${oldFirstName}1")
        val (_, response, updateResult) = Fuel.request(UsersApi.updateCurrentUser(userUpdate, globalSettings))
                .responseObject(User.Deserializer())
        TestCase.assertEquals(response.statusCode, 200)
        TestCase.assertEquals(updateResult.get().name, userUpdate.name)
        val (_, _, updatedUser) = Fuel.request(UsersApi.getCurrentUser(globalSettings))
                .responseObject(User.Deserializer())
        TestCase.assertEquals(updatedUser.get().name, userUpdate.name)
        Fuel.request(UsersApi.updateCurrentUser(originalUser, globalSettings)).response()
    }

    @Test
    fun testGetUser() {
        val (_, user2Response, user2Result) = Fuel.request(UsersApi.getUser(user2Guid, globalSettings))
                .responseObject(User.Deserializer())
        println(user2Result.get())
        TestCase.assertEquals(user2Response.statusCode, 200)
        val (_, user3Response, user3Result) = Fuel.request(UsersApi.getUser(user3Guid, globalSettings))
                .responseObject(User.Deserializer())
        println(user3Result.get())
        TestCase.assertEquals(user3Response.statusCode, 200)
    }

    @Test
    fun testGetContacts() {
        val (_, user2Response, user2Result) = Fuel.request(UsersApi.getContacts(user1Guid, globalSettings))
                .responseObject(Person.ArrayDeserializer())
        user2Result.get().forEach { println(it.name) }
        TestCase.assertEquals(user2Response.statusCode, 200)
    }

    @Test
    fun testGetUserPostsOnePage() {
        val (_, response, result) = Fuel.request(UsersApi.getPosts(user2Guid, globalSettings))
                .responseObject(Post.ArrayDeserializer())
        result.get().forEach { println("${it.post_type}(${it.guid}: (${it.title}) ${it.body}") }
        println(PageDataUrls.fromHeaderLinkField(response.headers))
        TestCase.assertEquals(response.statusCode, 200)
    }

    @Test
    fun testGetUserPostsManyPage() {
        var count = 1
        val maxCount = 10
        val (_, response, result) = Fuel.request(UsersApi.getPosts(user2Guid, globalSettings))
                .responseObject(Post.ArrayDeserializer())
        result.get().forEach { println("${it.post_type}(${it.guid}: (${it.title}) ${it.body}") }
        TestCase.assertEquals(response.statusCode, 200)
        var pageData = PageDataUrls.fromHeaderLinkField(response.headers)
        var hasMore = pageData.next.isNotBlank()

        while(count <= maxCount && hasMore)
        {
            println("[Page $count]")
            val (_, pResponse, pResult) = pageData.next.httpGet().responseObject(Post.ArrayDeserializer())
            pResult.get().forEach { println("${it.post_type}(${it.guid}: (${it.title}) ${it.body}") }
            pageData = PageDataUrls.fromHeaderLinkField(pResponse.headers)
            hasMore = pageData.next.isNotBlank()
            count++
        }
    }

    @Test
    fun testGetUserPhotos() {
        val (_, response, result) = Fuel.request(UsersApi.getPhotos(user2Guid, globalSettings))
                .responseObject(Photo.ArrayDeserializer())
        result.get().forEach { println("${it.dimensions}: ${it.sizes.medium}") }
        TestCase.assertEquals(response.statusCode, 200)
    }
}