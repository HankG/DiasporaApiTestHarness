package hankg.diaspora.api

import com.github.kittinunf.fuel.Fuel
import junit.framework.TestCase
import org.junit.Test
import java.time.LocalDateTime
import java.time.ZonedDateTime
import java.util.*

class TestNotificationsApi {

    @Test
    fun testListAllNotifications() {
        val (_, response, result) = Fuel.request(NotificationsApi.list(settings = globalSettings)).responseObject(Notification.ArrayDeserializer())
        TestCase.assertEquals(response.statusCode, 200)
        val notifications = result.get()
        println("# Notifications: ${notifications.size}")
        notifications.forEach {
            val baseString = "${it.type} ${it.read} : (" + it.event_creators.map {it.name} + ") on ${it.target.guid}"
            println (baseString)
        }

        val (_, nfresponse, nfresult) = Fuel.request(NotificationsApi.list(onlyAfter = true, afterDate = ZonedDateTime.now().plusYears(20), settings = globalSettings)).responseObject(Notification.ArrayDeserializer())
        TestCase.assertEquals(nfresponse.statusCode, 200)
        TestCase.assertTrue(nfresult.get().isEmpty())
    }

    @Test
    fun testGetInfo() {
        val (_, lresponse, lresult) = Fuel.request(NotificationsApi.list(settings = globalSettings)).responseObject(Notification.ArrayDeserializer())
        TestCase.assertEquals(lresponse.statusCode, 200)
        val notification = lresult.get()[0]

        val (request, response, result) = Fuel.request(NotificationsApi.getInfo(notification.guid, settings = globalSettings)).responseObject(Notification.Deserializer())
        println(request)
        TestCase.assertEquals(response.statusCode, 200)
        val notificationDetails = result.get()
        println(notificationDetails)
    }

    @Test
    fun testUpdateReadStatus() {
        val (request1, _, initial) = Fuel.request(NotificationsApi.list(settings = globalSettings)).responseObject(Notification.ArrayDeserializer())
        println(request1)
        val notification = initial.get()[0]

        val (request, response, _) = Fuel.request(NotificationsApi.markReadUnread(notification.guid, !notification.read, globalSettings)).response()
        println(request)
        TestCase.assertEquals(response.statusCode, 204)

        val (_, _, secondResult) = Fuel.request(NotificationsApi.list(settings = globalSettings)).responseObject(Notification.ArrayDeserializer())
        TestCase.assertTrue(secondResult.get()[0].read != notification.read)

        val (_, response2, _) = Fuel.request(NotificationsApi.markReadUnread(notification.guid, notification.read, globalSettings)).response()
        TestCase.assertEquals(response2.statusCode, 204)

        val (_, _, thirdResult) = Fuel.request(NotificationsApi.list(settings = globalSettings)).responseObject(Notification.ArrayDeserializer())
        TestCase.assertEquals(thirdResult.get()[0].read, notification.read)

    }

}