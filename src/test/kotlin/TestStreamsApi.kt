package hankg.diaspora.api

import com.github.kittinunf.fuel.Fuel
import junit.framework.TestCase
import org.junit.Test

class TestStreamsApi {

    @Test
    fun testGetActivityStream() {
        val (_, response, result) = Fuel.request(StreamsApi.getActivityStream(globalSettings))
                .responseObject(Post.ArrayDeserializer())
        TestCase.assertEquals(response.statusCode, 200)
        result.get().forEach { println("${it.author.name}: ${it.body}") }
        println(PageDataUrls.fromHeaderLinkField(response.headers))
    }

    @Test
    fun testGetAspectsStream() {
        val (_, response, result) = Fuel.request(StreamsApi.getAspectsStream(settings = globalSettings))
                .responseObject(Post.ArrayDeserializer())
        TestCase.assertEquals(response.statusCode, 200)
        result.get().forEach { println("${it.author.name}: ${it.body}") }
        println(PageDataUrls.fromHeaderLinkField(response.headers))

        val familyAspect = Aspect(2, "Friends", 0)
        val aspects = listOf<Aspect>(familyAspect)
        val (_, responseFamily, resultFamily) = Fuel.request(StreamsApi.getAspectsStream(aspects, globalSettings))
                .responseObject(Post.ArrayDeserializer())
        TestCase.assertEquals(responseFamily.statusCode, 200)
        resultFamily.get().forEach { println("${it.author.name}: ${it.body}") }
        println(PageDataUrls.fromHeaderLinkField(response.headers))
    }


    @Test
    fun testGetCommentedStream() {
        val (_, response, result) = Fuel.request(StreamsApi.getCommentedStream(globalSettings))
                .responseObject(Post.ArrayDeserializer())
        TestCase.assertEquals(response.statusCode, 200)
        result.get().forEach { println("${it.author.name}: ${it.body}") }
        println(PageDataUrls.fromHeaderLinkField(response.headers))
    }


    @Test
    fun testGetLikedStream() {
        val (_, response, result) = Fuel.request(StreamsApi.getLikedStream(globalSettings))
                .responseObject(Post.ArrayDeserializer())
        TestCase.assertEquals(response.statusCode, 200)
        result.get().forEach { println("${it.author.name}: ${it.body}") }
        println(PageDataUrls.fromHeaderLinkField(response.headers))
    }

    @Test
    fun testGetMainStream() {
        val (_, response, result) = Fuel.request(StreamsApi.getMainStream(globalSettings))
                .responseObject(Post.ArrayDeserializer())
        TestCase.assertEquals(response.statusCode, 200)
        result.get().forEach { println("${it.author.name}: ${it.body}") }
        println(PageDataUrls.fromHeaderLinkField(response.headers))
    }

    @Test
    fun testGetMentionsStream() {
        val (_, response, result) = Fuel.request(StreamsApi.getMentionsStream(globalSettings))
                .responseObject(Post.ArrayDeserializer())
        TestCase.assertEquals(response.statusCode, 200)
        result.get().forEach { println("${it.author.name}: ${it.body}") }
        println(PageDataUrls.fromHeaderLinkField(response.headers))
    }

    @Test
    fun testGetTagsStream() {
        val (_, response, result) = Fuel.request(StreamsApi.getTagsStream(globalSettings))
                .responseObject(Post.ArrayDeserializer())
        TestCase.assertEquals(response.statusCode, 200)
        result.get().forEach { println("${it.author.name}: ${it.body}") }
    }

}