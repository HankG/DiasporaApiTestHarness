package hankg.diaspora.api

import com.github.kittinunf.fuel.Fuel
import junit.framework.TestCase
import org.junit.Test

class TestAuthentication {

    @Test
    fun testRenewal() {
        val refresh_token=""
        val client_id=""
        val client_secret=""

        val (_, response, result) = Fuel.request(AuthenticationApi.refresh(refresh_token, client_id, client_secret, globalSettings))
                .responseObject(OpenIdAccessTokens.Deserializer())
        println(result.get())
        TestCase.assertEquals(response.statusCode, 200)
    }
}