package hankg.diaspora.api

import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.DataPart
import com.github.kittinunf.fuel.core.FuelError
import junit.framework.TestCase
import org.junit.Test
import java.io.File
import java.lang.IllegalArgumentException
import java.rmi.ServerError
import java.time.LocalDate
import java.time.LocalDateTime

class TestPostLifecycle {

    @Test
    fun testCreateFullPostWithPic() {
        val (_, _, aspectResult) = Fuel.request(AspectsApi.listAspects(globalSettings)).responseObject(Aspect.ArrayDeserializer())
        val familyAspect = aspectResult.get().find { it.name == "Family"}
        if (familyAspect == null) throw IllegalArgumentException("Need to have a family aspect")
        val aspects = listOf<Aspect>(familyAspect)
        val uploader = PhotosApi.upload(pending = true, profilePic = false, settings = globalSettings)
        val (_, photo_response, photo_result) = Fuel.upload(uploader.fullPath, uploader.method, uploader.params)
                .dataParts { _, _ ->
                    listOf(
                            DataPart(File("/tmp/laptops.jpeg"), name = "image", type="image/jpg")
                    )
                }.responseObject(Photo.Deserializer())
        TestCase.assertEquals(photo_response.statusCode, 200)
        val newPhoto = photo_result.get()

        val poll = Poll(question = "Do you like it?",
                poll_answers = listOf<Poll.Answer>(Poll.Answer(answer = "yes"), Poll.Answer(answer = "no"),
                        Poll.Answer(answer = "maybe")))
        val location = Location("Miami, FL", 25.7548, -80.1785)
        val newPost = Post(body = "I have a new laptop as of ${LocalDateTime.now()}! @{user1@localhost:3000} recommended it.",
                public = false, poll = poll, location = location, photos = listOf(newPhoto))
        val (_, response, result) = Fuel.request(PostsApi.addPost(newPost, aspects, globalSettings))
                .responseObject(Post.Deserializer())

        if(response.statusCode == 200) {
            println (result.get())
        } else {
            println("Error creating post ${response.statusCode}:${response.responseMessage}")
        }

    }

    @Test
    fun testLifecycle() {
        val post = createPost()
        println("Created post: $post")
        printPostActivity(post)
        val comment = commentOnPost(post)
        likePost(post)
        printPostActivity(post)
        println("Pulled Post after activity: ${getPost(post.guid)}")
        unlikePost(post)
        removePostComment(post, comment)
        printPostActivity(post)
        println("Pulled Post after removed activity: ${getPost(post.guid)}")
        deletePost(post)
        try {
            getPost(post.guid)
        } catch (e:IllegalArgumentException) {
            println("Post couldn't be found as expected")
        }
    }


    private fun createPost(text:String="Post at ${LocalDateTime.now()}", public:Boolean=true):Post {
        val post = Post(body = text, public = public)
        val (_, response, result) = Fuel.request(PostsApi.addPost(post, settings = globalSettings))
                .responseObject(Post.Deserializer())

        if(response.statusCode == 200) {
            return result.get()
        } else {
            throw IllegalArgumentException("Error creating post ${response.statusCode}:${response.responseMessage}")
        }
    }

    private fun deletePost(post:Post) {
        val (_, response, _) = Fuel.request(PostsApi.deletePost(post.guid, settings = globalSettings)).response()

        if(response.statusCode != 204) {
            throw IllegalArgumentException("Error deleting post ${response.statusCode}:${response.responseMessage}")
        }
    }

    private fun commentOnPost(post:Post, commentText:String="New comment at ${LocalDateTime.now()}"):Comment {
        val (_, response, result) = Fuel.request(CommentsApi.addComment(post.guid, commentText, globalSettings))
                .responseObject(Comment.Deserializer())
        if(response.statusCode == 201) {
            return result.get()
        } else {
            throw IllegalArgumentException("Error commenting on post ${response.statusCode}:${response.responseMessage}")
        }
    }

    private fun getComments(post:Post):List<Comment> {
        val (_, response, result) = Fuel.request(CommentsApi.getPostComments(post.guid, globalSettings))
                .responseObject(Comment.ArrayDeserializer())
        if(response.statusCode == 200) {
            return result.get()
        } else {
            throw IllegalArgumentException("Error getting post comments ${response.statusCode}:${response.responseMessage}")
        }
    }

    private fun getLikes(post:Post):List<Like> {
        val (_, response, result) = Fuel.request(LikesApi.showLikes(post.guid, globalSettings))
                .responseObject(Like.ArrayDeserializer())
        if(response.statusCode == 200) {
            return result.get()
        } else {
            throw IllegalArgumentException("Error getting post likes ${response.statusCode}:${response.responseMessage}")
        }
    }

    private fun getPost(postGuid:String):Post {
        val (_, response, result) = Fuel.request(PostsApi.getPost(postGuid, globalSettings))
                .responseObject(Post.Deserializer())
        if(response.statusCode == 200) {
            return result.get()
        } else {
            throw IllegalArgumentException("Error getting post ${response.statusCode}:${response.responseMessage}")
        }
    }

    private fun likePost(post:Post):Boolean {
        val (_, response, _) = Fuel.request(LikesApi.addLike(post.guid, globalSettings)).responseObject(Like.ArrayDeserializer())
        if(response.statusCode == 204) {
            return true
        } else {
            throw IllegalArgumentException("Error liking post ${response.statusCode}:${response.responseMessage}")
        }
    }

    private fun printPostActivity(post:Post) {
        val comments = getComments(post)
        println("# Comments: ${comments.size}")
        comments.forEach { println("${it.author.name}: ${it.body}") }
        val likes = getLikes(post)
        println("# likes: ${likes.size}")
        likes.forEach { println(it.author.name) }
    }

    private fun removePostComment(post:Post, comment:Comment) {
        val (_, response, _) = Fuel.request(CommentsApi.deleteComment(post.guid, comment.guid, globalSettings))
                .response()
        if(response.statusCode != 204) {
            throw IllegalArgumentException("Error commenting on post ${response.statusCode}:${response.responseMessage}")
        }
    }

    private fun unlikePost(post:Post):Boolean {
        val (_, response, _) = Fuel.request(LikesApi.removeLike(post.guid, globalSettings)).responseObject(Like.ArrayDeserializer())
        if(response.statusCode == 204) {
            return true
        } else {
            throw IllegalArgumentException("Error liking post ${response.statusCode}:${response.responseMessage}")
        }
    }

}